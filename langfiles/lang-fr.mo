��            )         �     �  (   �     �  *   �  (   (     Q     _  :   v  &   �  <   �  :     ,   P  8   }     �     �  >   �  .   +  _   Z  >   �     �     �          4     N     e     j     �  /   �     �     �  C  �     	  )   	     H	  /   ]	  )   �	     �	     �	  ;   �	  &   
  B   A
  ;   �
  F   �
  G     $   O      t  @   �  0   �  c     @   k     �  +   �  &   �  '     G   +     s  '        �  <   �     �     �                 
                                            	                                                                           %s characters %s lowercase letter %s lowercase letters %s number %s numbers %s special character %s special characters %s uppercase letter %s uppercase letters Access denied At least %s characters At least %s lowercase letter At least %s lowercase letters At least %s number At least %s numbers At least %s special character At least %s special characters At least %s uppercase letter At least %s uppercase letters Change password has been taken into account. Do you want to force all user to change their password ? Force change password Minimum lenght of the password  Minimum number of lowercase letter [a b c d e f g h ... x y z] Minimum number of number [0 1 2 3 4 5 6 7 8 9] Minimum number of special characters [~ ! @ # $ % ^ & * ( ) - _ = + [ ] { } ; : , . < > / ? | ] Minimum number of uppercase letter [A B C D E F G H ... X Y Z] No Password complexity policy Password complexity settings Password must be at least Password policy update Save Security policy parameter Security rules The character "%s" is forbidden in the password Yes and Project-Id-Version: security_policy
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-04-23 14:38+0100
PO-Revision-Date: 2015-04-23 15:30+0100
Last-Translator: Antoine GALLET <antoine.gallet@cantico.fr>
Language-Team: Cantico <antoine.gallet@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: spolicy_translate;spolicy_translate:1,2
X-Generator: Poedit 1.6.10
X-Poedit-SearchPath-0: programs
 %s caractères %s lettre minuscule %s lettres minuscules %s nombre %s nombres %s caractère spécial %s caractères spéciaux %s lettre majuscule %s lettres majuscules Accès refusé Au moins %s caractères Au moins %s lettre minuscule Au moins %s lettres minuscules Au moins %s nombre Au moins %s nombres Au moins %s caractère spéciale Au moins %s caractères spéciaux Au moins %s lettre majuscule Au moins %s lettres majuscules La demande de changement de mot de passe a bien été prise en compte. Voulez vous forcer tous les utilisateurs à changer leur mot de passe ? Forcer le changement du mot de passe Longueur minimum du mot de passe Nombre minimum de lettres minuscules [a b c d e f g h ... x y z] Nombre minimum de chiffres [0 1 2 3 4 5 6 7 8 9] Nombre minimum de caractères spéciaux [~ ! @ # $ % ^ & * ( ) - _ = + [ ] { } ; : , . < > / ?  | ] Nombre minimum de lettres majuscules [A B C D E F G H ... X Y Z] Non Exigences de complexité des mots de passe  Options de complexité du mot de passe Le mot de passe doit comporter au moins  Les exigences de complexité des mots de passe ont été mises à jour Enregistrer Paramètres de stratégie de sécurité Règles de sécurité Le  caractère "%s" n'est pas autorisé dans le mot de passe Oui et 