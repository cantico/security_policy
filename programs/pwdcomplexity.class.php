<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/pwdcomplexity.class.php';
require_once dirname(__FILE__).'/functions.php';



class Func_PwdComplexity_SecurityPolicy extends Func_PwdComplexity {


    public function getDescription()
    {
        return bab_translate('Specific password complexity definityion');
    }

    /**
     * Register myself as a functionality.
     */
    public static function register()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $functionalities = new bab_functionalities();
        return $functionalities->registerClass('Func_PwdComplexity_SecurityPolicy', __FILE__);
    }

    /**
     * Unregister myself as a functionality.
     */
    public static function unregister()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $functionalities = new bab_functionalities();
        return $functionalities->unregister('PwdComplexity/SecurityPolicy');
    }

    public function registerPwdComplexity()
    {
        if (Func_PwdComplexity::register() === false) {
            return false;
        }
        return Func_PwdComplexity_SecurityPolicy::register();
    }


    public function isValid($pwd)
    {

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/security_policy');

        $number = $registry->getValue('number',0);
        $lowercase = $registry->getValue('lowercase',0);
        $uppercase = $registry->getValue('uppercase',0);
        $special = $registry->getValue('special',0);
        $pwd_length = $registry->getValue('pwd_length',6);

        $return = true;

        if (mb_strlen($pwd) < $pwd_length) {
            $this->error[]= sprintf(spolicy_translate("%s characters"),$pwd_length);
            $return = false;
        }

        if($number){
            $pattern = '[0-9].*';
            $currentPattern = '';
            for($i = 0; $i < $number; $i++){
                $currentPattern.= $pattern;
            }
            $currentPattern = '#'.$currentPattern.'#';
            if (!preg_match($currentPattern, $pwd)) {
                $this->error[]= sprintf(spolicy_translate("%s number", "%s numbers", $number),$number);
                $return = false;
            }
        }

        if($lowercase){
            $pattern = '[a-z].*';
            $currentPattern = '';
            for($i = 0; $i < $lowercase; $i++){
                $currentPattern.= $pattern;
            }
            $currentPattern = '#'.$currentPattern.'#';
            if (!preg_match($currentPattern, $pwd)) {
                $this->error[]= sprintf(spolicy_translate("%s lowercase letter", "%s lowercase letters", $lowercase),$lowercase);
                $return = false;
            }
        }

        if($uppercase){
            $pattern = '[A-Z].*';
            $currentPattern = '';
            for($i = 0; $i < $uppercase; $i++){
                $currentPattern.= $pattern;
            }
            $currentPattern = '#'.$currentPattern.'#';
            if (!preg_match($currentPattern, $pwd)) {
                $this->error[]= sprintf(spolicy_translate("%s uppercase letter", "%s uppercase letters", $uppercase),$uppercase);
                $return = false;
            }
        }

        if($special){
            $pattern = '[\~\!\@\#\$\%\^\&\*\(\)\-\_\=\+\[\]\{\}\;\:\,\.\<\>\/\?\|].*';
            $currentPattern = '';
            for($i = 0; $i < $special; $i++){
                $currentPattern.= $pattern;
            }
            $currentPattern = '#'.$currentPattern.'#';
            if (!preg_match($currentPattern, $pwd)) {
                $this->error[]= sprintf(spolicy_translate("%s special character", "%s special characters", $special),$special);
                $return = false;
            }
        }

        $pattern = '#[^\~\!\@\#\$\%\^\&\*\(\)\-\_\=\+\[\]\{\}\;\:\,\.\<\>\/\?\|a-zA-Z0-9]+#';
        if (preg_match($pattern, $pwd, $match)) {
            $this->error[]= sprintf(spolicy_translate('The character "%s" is forbidden in the password'),$match[0]);
            $return = false;
        }

        return $return;
    }

    public function getRules()
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/security_policy');

        $number = $registry->getValue('number',0);
        $lowercase = $registry->getValue('lowercase',0);
        $uppercase = $registry->getValue('uppercase',0);
        $special = $registry->getValue('special',0);
        $pwd_length = $registry->getValue('pwd_length',6);

        $rules = array(
            array(
                'preg' => '[\~\!\@\#\$\%\^\&\*\(\)\-\_\=\+\[\]\{\}\;\:\,\.\<\>\/\?\|a-zA-Z0-9]{'.$pwd_length.',}',
                'text' => sprintf(spolicy_translate("At least %s characters"),$pwd_length)
            )
        );

        if($number){
            $pattern = '[0-9].*';
            $currentPattern = '';
            for($i = 0; $i < $number; $i++){
                $currentPattern.= $pattern;
            }
            $rules[] = array(
                'preg' => $currentPattern,
                'text' => sprintf(spolicy_translate("At least %s number", "At least %s numbers", $number),$number)
            );
        }

        if($lowercase){
            $pattern = '[a-z].*';
            $currentPattern = '';
            for($i = 0; $i < $lowercase; $i++){
                $currentPattern.= $pattern;
            }
            $rules[] = array(
                'preg' => $currentPattern,
                'text' => sprintf(spolicy_translate("At least %s lowercase letter", "At least %s lowercase letters", $lowercase),$lowercase)
            );
        }

        if($uppercase){
            $pattern = '[A-Z].*';
            $currentPattern = '';
            for($i = 0; $i < $uppercase; $i++){
                $currentPattern.= $pattern;
            }
            $rules[] = array(
                'preg' => $currentPattern,
                'text' => sprintf(spolicy_translate("At least %s uppercase letter", "At least %s uppercase letters", $uppercase),$uppercase)
            );
        }

        if($special){
            $pattern = '[\~\!\@\#\$\%\^\&\*\(\)\-\_\=\+\[\]\{\}\;\:\,\.\<\>\/\?\|].*';
            $currentPattern = '';
            for($i = 0; $i < $special; $i++){
                $currentPattern.= $pattern;
            }
            $rules[] = array(
                'preg' => $currentPattern,
                'text' => sprintf(spolicy_translate("At least %s special character", "At least %s special characters", $special),$special)
            );
        }

        return $rules;
    }

    public function getRandomPwd($length = 0)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/security_policy');

        $number = $registry->getValue('number',0);
        $lowercase = $registry->getValue('lowercase',0);
        $uppercase = $registry->getValue('uppercase',0);
        $special = $registry->getValue('special',0);
        $pwd_length = $registry->getValue('pwd_length',6);

        if($length < $pwd_length){
            $length = $pwd_length;
        }

        $minLength = $number+$lowercase+$uppercase+$special;

        $complement = $length-$minLength;
        if($complement < 0){
            $complement = 0;
        }

        // Mask Rules
        // # - digit
        // C - Caps Character (A-Z)
        // c - Small Character (a-z)
        // ! - Custom Extended Characters
        // w - What ever

        for($i = 0; $i < $lowercase; $i++) {
            $mask[] = 'c';
        }

        for($i = 0; $i < $uppercase; $i++) {
            $mask[] = 'C';
        }

        for($i = 0; $i < $number; $i++) {
            $mask[] = '#';
        }

        for($i = 0; $i < $special; $i++) {
            $mask[] = '!';
        }

        for($i = 0; $i < $complement; $i++) {
            $mask[] = 'w';
        }

        shuffle($mask);

        $pwd = '';

        $lowChar = 'abcdefghijkmnpqrstwxyz';
        $upChar = 'ABCDEFGHJKLMNPQRSTWXYZ';
        $numChar = '23456789';
        $specialChar = '!@#$%&*()-_=+[]{};:,.<>/?|';
        $whevChar = $lowChar.$upChar.$numChar.$specialChar;

        foreach($mask as $m) {
            switch($m) {
                case '#':
                    $pwd .= $numChar[(rand() % strlen($numChar))];
                    break;

                case 'c':
                    $pwd .= $lowChar[(rand() % strlen($lowChar))];
                    break;

                case 'C':
                    $pwd .= $upChar[(rand() % strlen($upChar))];
                    break;

                case '!':
                    $pwd .= $specialChar[(rand() % strlen($specialChar))];
                    break;

                case 'w':
                    $pwd .= $whevChar[(rand() % strlen($whevChar))];
                    break;
            }
        }

        return $pwd;
    }

    public function getErrorDescription()
    {
        $last = false;
        if(count($this->error) > 1){
            $last = array_pop($this->error);
        }
        $str = implode(", ", $this->error);
        if(trim($str) != ''){
            $str = spolicy_translate("Password must be at least").' '.$str;
            if($last){
                $str.= ' '.spolicy_translate('and').' '.$last;
            }
            $str.='.';
        }
        return $str;
    }

}
