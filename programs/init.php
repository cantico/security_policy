<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include "base.php";


function security_policy_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath']."utilit/eventincl.php";

    bab_removeAddonEventListeners('security_policy');
    require_once dirname(__FILE__).'/pwdcomplexity.class.php';
    Func_PwdComplexity_SecurityPolicy::unregister();

    $func = new bab_functionalities();
    $func->copyToParent('PwdComplexity/DefaultPortal');

    return true;
}


function security_policy_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    require_once dirname(__FILE__).'/functions.php';

    bab_removeAddonEventListeners('security_policy');

    $addon = bab_getAddonInfosInstance('security_policy');
    $addon->addEventListener('bab_eventBeforeSiteMapCreated',	'security_policy_onSiteMapItems',	'init.php');

    require_once dirname(__FILE__).'/pwdcomplexity.class.php';
    Func_PwdComplexity_SecurityPolicy::register();

    $func = new bab_functionalities();
    $func->copyToParent('PwdComplexity/SecurityPolicy');

    return true;
}




/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function security_policy_onSiteMapItems(bab_eventBeforeSiteMapCreated $event) {
    require_once dirname(__FILE__).'/functions.php';
    bab_functionality::includefile('Icons');
    if (bab_isUserAdministrator()) {
        $link = $event->createItem('security_policy');
        $link->setLabel(spolicy_translate('Security policy parameter'));
        $link->setLink('?tg=addon/security_policy/main&idx=security.rules');
        $link->setPosition(array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons'));
        $link->addIconClassname(Func_Icons::APPS_PREFERENCES_AUTHENTICATION);

        $event->addFunction($link);
    }

}
