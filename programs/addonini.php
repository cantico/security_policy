;<?php /*

[general]
name							="security_policy"
version							="0.4.1"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Securiy policy and password complexity"
description.fr					="Politique de sécurité et complixté des mots de passes"
long_description.fr             ="README.md"
delete							=1
ov_version						="8.2.96"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
icon							="icon.png"


[addons]

widgets							="1.0.44"
LibTranslate					=">=1.12.0rc3.01"

;*/