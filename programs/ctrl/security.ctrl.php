<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/security.ui.php';

bab_functionality::includeOriginal('Icons');


/**
 *
 */
class spolicy_CtrlSecurity extends spolicy_Controller
{

    public function rules()
    {
        if (!spolicy_isManager())
        {
            throw new bab_AccessException(spolicy_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet('addons/security_policy/security_policy.css');

        $page->addItemMenu('displayUsersGroup', spolicy_translate('Security rules'), $this->proxy()->rules()->url());
        $page->addItemMenu('forceChangePwd', spolicy_translate('Force change password'), $this->proxy()->changePwdConfirm()->url());
        $page->setCurrentItemMenu('displayUsersGroup');

        $editor = new security_policy_ruleEditor();

        $page->setTitle(spolicy_translate('Password complexity settings'));

        $page->addItem($editor);

        return $page;
    }

    public function save($rules = null)
    {
        if (!spolicy_isManager())
        {
            throw new bab_AccessException(spolicy_translate('Access denied'));
        }

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/security_policy');
        $registry->setKeyValue('number', $rules['number']);
        $registry->setKeyValue('lowercase', $rules['lowercase']);
        $registry->setKeyValue('uppercase', $rules['uppercase']);
        $registry->setKeyValue('special', $rules['special']);
        $registry->setKeyValue('pwd_length', $rules['pwd_length']);

        $GLOBALS['babBody']->addNextPageMessage(spolicy_translate('Password policy update'));

        return true;
    }

    public function changePwdConfirm()
    {
        if (!spolicy_isManager())
        {
            throw new bab_AccessException(spolicy_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet('addons/security_policy/security_policy.css');

        $page->addItemMenu('displayUsersGroup', spolicy_translate('Security rules'), $this->proxy()->rules()->url());
        $page->addItemMenu('forceChangePwd', spolicy_translate('Force change password'), $this->proxy()->changePwdConfirm()->url());
        $page->setCurrentItemMenu('forceChangePwd');

        $page->setTitle(spolicy_translate('Force change password'));

        $page->addItem(
            $W->VBoxItems(
                $W->Label(spolicy_translate('Do you want to force all user to change their password ?')),
                $W->HBoxItems(
                    $W->Link(spolicy_translate('Yes'),spolicy_Controller()->security()->forceChangePwd())->addClass('widget-strong'),
                    $W->Label(' | '),
                    $W->Link(spolicy_translate('No'),spolicy_Controller()->security()->rules())->addClass('widget-strong')
                )->setHorizontalSpacing(1, 'em')
            )
            ->addClass('BabLoginMenuBackground')
            ->addClass('widget-bordered')
        );

        return $page;
    }

    public function forceChangePwd()
    {
        if (!spolicy_isManager())
        {
            throw new bab_AccessException(spolicy_translate('Access denied'));
        }
        $users = bab_getGroupsMembers(array(BAB_REGISTERED_GROUP), true, true);
        foreach($users as $user){
            bab_updateUserById($user['id'], array('force_pwd_change' => true), $error);
        }

        $GLOBALS['babBody']->addNextPageMessage(spolicy_translate('Change password has been taken into account.'));
        spolicy_Controller()->security()->rules()->location();
    }
}


