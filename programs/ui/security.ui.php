<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

bab_Widgets()->includePhpClass('Widget_Form');

class security_policy_ruleEditor extends Widget_Form
{

    public function __construct()
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->setName('rules');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();

        $this->colon(true);

        $this->addItem(
            $W->SubmitButton()
                ->setAction(spolicy_Controller()->security()->save())
                ->setSuccessAction(spolicy_Controller()->security()->rules())
                ->setFailedAction(spolicy_Controller()->security()->rules())
                ->setLabel(spolicy_translate('Save'))
        );

        $this->loadValues();
    }


    protected function loadValues()
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/security_policy');
        $this->setValue(array('rules', 'number'), $registry->getValue('number',0));
        $this->setValue(array('rules', 'lowercase'), $registry->getValue('lowercase',0));
        $this->setValue(array('rules', 'uppercase'), $registry->getValue('uppercase',0));
        $this->setValue(array('rules', 'special'), $registry->getValue('special',0));
        $this->setValue(array('rules', 'pwd_length'), $registry->getValue('pwd_length',6));
    }

    protected function number()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            spolicy_translate('Minimum number of number [0 1 2 3 4 5 6 7 8 9]'),
            $W->LineEdit()->setName('number')
        );
    }

    protected function lowercase()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            spolicy_translate('Minimum number of lowercase letter [a b c d e f g h ... x y z]'),
            $W->LineEdit()->setName('lowercase')
        );
    }

    protected function uppercase()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            spolicy_translate('Minimum number of uppercase letter [A B C D E F G H ... X Y Z]'),
            $W->LineEdit()->setName('uppercase')
        );
    }

    protected function special()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            spolicy_translate('Minimum number of special characters [~ ! @ # $ % ^ & * ( ) - _ = + [ ] { } ; : , . < > / ? | ]'),
            $W->LineEdit()->setName('special')
        );
    }

    protected function pwd_length()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            spolicy_translate('Minimum lenght of the password '),
            $W->LineEdit()->setName('pwd_length')
        );
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->Title(spolicy_translate('Password complexity policy'))
        );
        $this->addItem(
            $this->number()
        );
        $this->addItem(
            $this->lowercase()
        );
        $this->addItem(
            $this->uppercase()
        );
        $this->addItem(
            $this->special()
        );
        $this->addItem(
            $this->pwd_length()
        );
    }
}
